package com.twuc.webApp;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Controller {
    //2.1
    @RequestMapping(value="/api/users/{userId)")
    public String getId(@PathVariable Integer userId){
        return userId.toString();
    }

    @RequestMapping(value="/api/users2/{userId)")
    public String getId2(@PathVariable int userId){
        return String.valueOf(userId);
    }


    @RequestMapping(value="/api/users/{userId}/books/{bookId}")
    public String getId2(@PathVariable String userId,@PathVariable String bookId){
        return userId + " " + bookId;
    }
    //2.2

    @RequestMapping(value="/api/users3")
    public String getVal(@RequestParam("id") String id){
        return id;
    }
    @RequestMapping(value="/api/users4")
    public String getDefaultVal(@RequestParam(value = "id",defaultValue = "default value") String id){
        return id;
    }
    @RequestMapping(value="/api/users5")
    public String getDefaultVal(@RequestParam List<String> ids){
        return ids.get(2);
    }

}
