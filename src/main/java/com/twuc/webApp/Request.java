package com.twuc.webApp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Request {

    @RequestMapping("/test")
    public @ResponseBody String greeting2(String gender, @RequestParam(name = "class") Integer num) {
        return num.toString();
    }
}
