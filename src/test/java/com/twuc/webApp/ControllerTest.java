package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTest {
    @Autowired
    MockMvc mockMvc;

    //2.1
    @Test
    public void integerTypeTest() throws Exception {
        mockMvc.perform(get("/api/users/123")).andExpect(content().string("123"));
    }

    @Test
    public void intTypeTest() throws Exception {
        mockMvc.perform(get("/api/users2/123")).andExpect(content().string("123"));
    }

    @Test
    public void mutiParamTest() throws Exception {
        mockMvc.perform(get("/api/users/123/books/456")).andExpect(content().string("123 456"));
    }

    //2.2
    @Test
    public void simpleTest() throws Exception {
        mockMvc.perform(get("/api/users3?id=123")).andExpect(content().string("123"));
    }

    @Test
    public void defaultValTest() throws Exception {
        mockMvc.perform(get("/api/users4")).andExpect(content().string("default value"));
    }

    @Test
    public void collectorTest() throws Exception {
        mockMvc.perform(get("/api/users5?ids=1,2,3")).andExpect(content().string("3"));
    }
}
