package com.twuc.webApp;
//这是课上作业 09。17


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class RequestTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    public void test3_1() throws Exception {
        //mockMvc.perform(get("/test?num=12")).andExpect(content().string("12"));
        mockMvc.perform(get("/test?num=12")).andExpect(status().is4xxClientError());
    }

    @Test
    public void test3_2() throws Exception {
        //mockMvc.perform(get("/test?num=12")).andExpect(content().string("12"));
        mockMvc.perform(get("/test?num=12")).andExpect(status().is4xxClientError());
    }
}




